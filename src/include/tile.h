#ifndef TILE_H
#define TILE_H

#include <SFML/Graphics.hpp>

class Tile
{
    private:
        // Type of tile
        short m_Type;

        // Where is the tile?
        sf::Vector2f m_Position;

        // Sprite used
        sf::Sprite m_Sprite;

        // Is the tile active?
        bool m_Active=true;
    public:
        // Create a tile
        void create(int xPos, int yPos, int type);

        // Return a rectangle that is the position in the world
        sf::FloatRect getPosition();

        // Get a copy of the sprite to draw
        sf::Sprite getSprite();

        // Update the tile
        void update();

        // Get tile type
        short getType();

        // Set tile type
        void setType(short type);

        // Get tile activation status
        bool isActive();
};

#endif // TILE_H
