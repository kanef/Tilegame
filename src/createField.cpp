#include <SFML/Graphics.hpp>
#include "include/main.h"
#include "include/tile.h"
#include <cstdlib>
#include <ctime>

Tile* createField(sf::IntRect field)
{
    // How big is each tile/texture
    const int TILE_SIZE = 32;

    Tile* tiles = new Tile[field.width * field.height];

    srand((int)time(0));
    int x = 0, y = 0;
    short type = 0;

    for(int i = 0; i < (field.width * field.height); i++)
    {
        x = (i % field.width) * TILE_SIZE;
        y = (i / field.height) * TILE_SIZE;

        // Setting tile types: middle is person, rest are random
        if (i == (field.width * field.height) / 2) type = 14;
        else type = (rand() % 7) + 7;

        tiles[i].create(x, y, type);
    }

    return tiles;
}
